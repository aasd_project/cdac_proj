
# Display menu options
def display_menu():
    print("Choose an AWS service:")
    print("1. S3")
    print("2. EC2")
    print("3. RDS")
    print("0. Exit")

# Get user input 
def get_user_choice():
    choice = input("Enter your choice (0-5): ")
    return choice

# Main program loop
while True:
    display_menu()
    choice = get_user_choice()

    if choice == '1':
        import service_request
        service_request.list_services(choice)
        print("REQUEST FOR S3 SERVICE SEND FOR APPROVAL SUCCESSFULLY!!!")
    elif choice == '2':
        import service_request
        service_request.list_services(choice)
        print("REQUEST FOR EC2 SERVICE SEND FOR APPROVAL SUCCESSFULLY!!!")
    elif choice == '3':
        import service_request
        service_request.list_services(choice)
        print("SREQUEST FOR RDS ERVICE SEND FOR APPROVAL SUCCESSFULLY!!!")
    
    elif choice == '0':
        print("Exiting program...")
        break
    else:
        print("Invalid choice. Please try again.")
