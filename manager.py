import service_request
user = "manager"
passa = "manager@123"
#function for the request option
def make_request():
    request = input("Please enter your request: ")
    print("Request submitted successfully.")
    
# function for the approve request option
def approve_request():
    request_id = input("Please enter the ID of the request to approve: ")
    print("Request approved successfully.")
    
# function for the cancel option
def cancel_request():
    request_id = input("Please enter the ID of the request to cancel: ")
    print("Request canceled successfully.")
    
# function for the logout option
def logout():
    print("Logging out...")
    
def login():
    username = str(input("Username: "))
    password = str(input("Password: "))
    if username == user and password == passa:
        mainM()
    else:
        print("Wrong username/password.")
        print("Try again")
        login()

# define the main function
def mainM():
    while True:
        print("Welcome, Manager ")
        print("1. Request")
        print("2. Approve request")
        print("3. Cancel request")
        print("4. Logout")
        
        choice = input("Please enter your choice: ")
        
        if choice == "1":
            service_request.services_for_approve()
        elif choice == "2":
            approve_request()
        elif choice == "3":
            cancel_request()
        elif choice == "4":
            logout()
            break
        else:
            print("Invalid choice. Please try again.")

# call the main function to start the program
login()